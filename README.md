# damdan

Repartition d'un applicatif sur plusieurs site

- applicatif dans container(s) docker
- donnees dans repertoire ou volume(s) docker

## site master

- gere syncro data avec slave(s)
- gere liste ip avec slave(s)
- monitoring slave(s)
- docker applicatif on

## site(s) slave

- envoie ip slave au master
- docker applicatif off

## gestion failover

- site(s) slave capable(s) de passer master si perte master

## dns

gestion nom de domaine sur gandi/ovh 

## test

{{ book.title }}

```uml
@startuml

	Class Stage
	Class Timeout {
		+constructor:function(cfg)
		+timeout:function(ctx)
		+overdue:function(ctx)
		+stage: Stage
	}
 	Stage <|-- Timeout

@enduml
```
